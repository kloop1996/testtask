package by.bsuir.kloop1996.testtask.viewModel;

/**
 * Created by kloop1996 on 15.10.2016.
 */

public interface ViewModel {
    void destroy();
}
