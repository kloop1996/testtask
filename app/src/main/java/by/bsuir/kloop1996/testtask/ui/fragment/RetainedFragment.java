package by.bsuir.kloop1996.testtask.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import by.bsuir.kloop1996.testtask.viewModel.DynamicFormViewModel;

/**
 * Created by kloop1996 on 16.10.2016.
 */

public class RetainedFragment extends Fragment {
    private DynamicFormViewModel data;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public DynamicFormViewModel getData() {
        return data;
    }

    public void setData(DynamicFormViewModel data) {
        this.data = data;
    }
}