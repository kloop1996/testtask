package by.bsuir.kloop1996.testtask.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by kloop1996 on 14.10.2016.
 */

public class Field implements Parcelable {
    public static final Creator<Field> CREATOR = new Creator<Field>() {
        public Field createFromParcel(Parcel source) {
            return new Field(source);
        }

        public Field[] newArray(int size) {
            return new Field[size];
        }
    };
    private String title;
    private String name;
    private String type;
    private String fieldValue;
    private HashMap<String, String> values;

    public Field(String title, String name, String type, String fieldValue, HashMap<String, String> values) {
        this.title = title;
        this.name = name;
        this.type = type;
        this.fieldValue = fieldValue;
        this.values = values;
    }

    public Field(Parcel in) {
        this.title = in.readString();
        this.name = in.readString();
        this.type = in.readString();
        this.fieldValue = in.readString();
        in.readMap(this.values, ClassLoader.getSystemClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flag) {
        dest.writeString(this.title);
        dest.writeString(this.name);
        dest.writeString(this.type);
        dest.writeString(this.fieldValue);
        dest.writeMap(values);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public HashMap<String, String> getValues() {
        return values;
    }

    public void setValues(HashMap<String, String> values) {
        this.values = values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Field field = (Field) o;

        if (title != null ? !title.equals(field.title) : field.title != null) return false;
        if (name != null ? !name.equals(field.name) : field.name != null) return false;
        if (type != null ? !type.equals(field.type) : field.type != null) return false;
        if (fieldValue != null ? !fieldValue.equals(field.fieldValue) : field.fieldValue != null)
            return false;
        return values != null ? values.equals(field.values) : field.values == null;

    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (fieldValue != null ? fieldValue.hashCode() : 0);
        result = 31 * result + (values != null ? values.hashCode() : 0);
        return result;
    }

}
