package by.bsuir.kloop1996.testtask.model;

/**
 * Created by kloop1996 on 16.10.2016.
 */

public class ResultOperation {
    private String result;

    public ResultOperation(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ResultOperation that = (ResultOperation) o;

        return result != null ? result.equals(that.result) : that.result == null;

    }

    @Override
    public int hashCode() {
        return result != null ? result.hashCode() : 0;
    }
}
