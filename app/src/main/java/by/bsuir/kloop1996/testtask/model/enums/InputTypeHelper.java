package by.bsuir.kloop1996.testtask.model.enums;

import android.text.InputType;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kloop1996 on 16.10.2016.
 */

public final class InputTypeHelper {
    private static Map<InputTypeName, Integer> inputTypes = new HashMap<InputTypeName, Integer>();

    static {
        inputTypes.put(InputTypeName.TEXT, InputType.TYPE_CLASS_TEXT);
        inputTypes.put(InputTypeName.NUMERIC, (InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL));
        inputTypes.put(InputTypeName.LIST, InputType.TYPE_CLASS_TEXT);
    }

    public static int getInputType(String inputType) {
        InputTypeName inputTypeName = InputTypeName.valueOf(inputType);

        if (inputTypeName != null) {
            return inputTypes.get(inputTypeName);
        } else {
            return InputType.TYPE_CLASS_TEXT;
        }
    }
}
