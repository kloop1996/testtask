package by.bsuir.kloop1996.testtask.injection.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by kloop1996 on 17.10.2016.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerDataManager {
}