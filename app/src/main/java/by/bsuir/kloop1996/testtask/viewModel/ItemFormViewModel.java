package by.bsuir.kloop1996.testtask.viewModel;

import android.content.Context;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;

import java.lang.reflect.Array;

import by.bsuir.kloop1996.testtask.model.enums.InputTypeHelper;
import by.bsuir.kloop1996.testtask.model.enums.InputTypeName;
import by.bsuir.kloop1996.testtask.model.Field;

/**
 * Created by kloop1996 on 15.10.2016.
 */

public class ItemFormViewModel implements ViewModel {

    private Context context;
    private Field field;

    private ObservableField<String> title;
    private ObservableBoolean isList;
    private ObservableInt inputType;


    public ItemFormViewModel(Context context, Field field) {
        this.context = context;
        this.field = field;

        title = new ObservableField<>(field.getTitle());
        isList = new ObservableBoolean();
        inputType = new ObservableInt(InputTypeHelper.getInputType(field.getType()));


        if (InputTypeName.LIST == InputTypeName.valueOf(field.getType())) {
            isList.set(true);

        } else {
            isList.set(false);
        }

    }

    public String getFieldValue() {
        return field.getFieldValue();
    }

    public void setFieldValue(String fieldValue) {
        field.setFieldValue(fieldValue);
    }

    public String[] getValues() {
        String[] result = null;

        if (field.getValues() != null) {
            result = new String[field.getValues().size()];
            result = field.getValues().values().toArray(result);
        }

        return result;
    }

    public ObservableField<String> getTitle() {
        return title;
    }

    public ObservableInt getInputType() {
        return inputType;
    }

    public ObservableBoolean isList() {
        return isList;
    }


    @Override
    public void destroy() {
        context = null;
    }


}
