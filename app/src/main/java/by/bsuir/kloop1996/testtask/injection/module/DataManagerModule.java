package by.bsuir.kloop1996.testtask.injection.module;

import by.bsuir.kloop1996.testtask.data.TestTaskService;
import by.bsuir.kloop1996.testtask.injection.scope.PerDataManager;
import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by kloop1996 on 17.10.2016.
 */

@Module
public class DataManagerModule {
    public DataManagerModule() {

    }

    @Provides
    @PerDataManager
    TestTaskService provideTestTaskService() {
        return TestTaskService.Factory.create();
    }

    @Provides
    @PerDataManager
    Scheduler provideSubscribeScheduler() {
        return Schedulers.io();
    }
}
