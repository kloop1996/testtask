package by.bsuir.kloop1996.testtask.model;

import java.util.HashMap;

/**
 * Created by kloop1996 on 16.10.2016.
 */

public class FormData {
    private HashMap<String, String> form;

    public FormData(HashMap<String, String> form) {
        this.form = form;
    }

    public HashMap<String, String> getForm() {
        return form;
    }

    public void setForm(HashMap<String, String> form) {
        this.form = form;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FormData that = (FormData) o;

        return form != null ? form.equals(that.form) : that.form == null;

    }

    @Override
    public int hashCode() {
        return form != null ? form.hashCode() : 0;
    }
}
