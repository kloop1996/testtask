package by.bsuir.kloop1996.testtask.util;

import java.util.Map;
import java.util.Objects;

/**
 * Created by kloop1996 on 17.10.2016.
 */

public class MapUtil {
    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }
}
