package by.bsuir.kloop1996.testtask.ui.activity;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import by.bsuir.kloop1996.testtask.Constants;
import by.bsuir.kloop1996.testtask.R;
import by.bsuir.kloop1996.testtask.adapter.FieldAdapter;
import by.bsuir.kloop1996.testtask.databinding.ActivityDynamicFormBinding;
import by.bsuir.kloop1996.testtask.model.Field;
import by.bsuir.kloop1996.testtask.ui.fragment.RetainedFragment;
import by.bsuir.kloop1996.testtask.util.DialogFactory;
import by.bsuir.kloop1996.testtask.util.NetworkUtils;
import by.bsuir.kloop1996.testtask.viewModel.DynamicFormViewModel;

public class DynamicFormActivity extends AppCompatActivity implements DynamicFormViewModel.DataListener {
    private ActivityDynamicFormBinding activityMainBinding;
    private DynamicFormViewModel dynamicFormViewModel;
    private RetainedFragment dataFragment;
    private Dialog errorDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fm = getSupportFragmentManager();
        dataFragment = (RetainedFragment) getSupportFragmentManager().findFragmentByTag(Constants.DATA_FRAGMENT);

        if (dataFragment == null) {
            dataFragment = new RetainedFragment();
            fm.beginTransaction().add(dataFragment, Constants.DATA_FRAGMENT).commit();
            dataFragment.setData(dynamicFormViewModel);
            dynamicFormViewModel = new DynamicFormViewModel(this, this);
            if (NetworkUtils.isNetworkAvailable(this))
                dynamicFormViewModel.loadDynamicForm();
            else {
                errorDialog = DialogFactory.createSimpleOkErrorDialog(this, getResources().getString(R.string.error_connection));
                errorDialog.show();
            }
        } else {
            dynamicFormViewModel = dataFragment.getData();
            dynamicFormViewModel.setContext(this);
            dynamicFormViewModel.setDataListener(this);
        }

        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_dynamic_form);
        activityMainBinding.setViewModel(dynamicFormViewModel);
        setupRecyclerView(activityMainBinding.fieldRecyclerView);

        if (savedInstanceState != null)
            dynamicFormViewModel.invalidate();
    }


    @Override
    public void onDynamicFormChanged(List<Field> fields) {
        FieldAdapter adapter =
                (FieldAdapter) activityMainBinding.fieldRecyclerView.getAdapter();
        adapter.setFields(fields);
        adapter.notifyDataSetChanged();
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        FieldAdapter adapter = new FieldAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (errorDialog != null && errorDialog.isShowing()) {
            errorDialog.cancel();
        }

        if (dynamicFormViewModel != null) {
            dynamicFormViewModel.destroy();
        }
        dataFragment.setData(dynamicFormViewModel);
    }
}
