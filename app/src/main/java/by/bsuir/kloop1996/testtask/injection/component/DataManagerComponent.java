package by.bsuir.kloop1996.testtask.injection.component;

import by.bsuir.kloop1996.testtask.data.DataManager;
import by.bsuir.kloop1996.testtask.injection.module.DataManagerModule;
import by.bsuir.kloop1996.testtask.injection.scope.PerDataManager;
import dagger.Component;

/**
 * Created by kloop1996 on 17.10.2016.
 */

@PerDataManager
@Component(dependencies = ApplicationComponent.class, modules = DataManagerModule.class)
public interface DataManagerComponent {

    void inject(DataManager dataManager);
}