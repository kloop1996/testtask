package by.bsuir.kloop1996.testtask.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import by.bsuir.kloop1996.testtask.model.enums.InputTypeName;
import by.bsuir.kloop1996.testtask.util.MapUtil;

/**
 * Created by kloop1996 on 14.10.2016.
 */

public class DynamicForm implements Parcelable {
    public static final Creator<DynamicForm> CREATOR = new Creator<DynamicForm>() {
        public DynamicForm createFromParcel(Parcel source) {
            return new DynamicForm(source);
        }

        public DynamicForm[] newArray(int size) {
            return new DynamicForm[size];
        }
    };
    private String title;
    @SerializedName("fields")
    private List<Field> fields = Collections.emptyList();

    public DynamicForm(String title, List<Field> fields) {
        this.title = title;
        this.fields = fields;
    }


    public DynamicForm(Parcel in) {
        this.title = in.readString();
        in.readList(this.fields, ClassLoader.getSystemClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flag) {
        dest.writeString(this.title);
        dest.writeList(this.fields);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    public FormData getFormData() {
        HashMap<String, String> formData = new HashMap<>();

        for (Field field : fields) {
            if (InputTypeName.valueOf(field.getType()) == InputTypeName.LIST)
                formData.put(field.getName(), MapUtil.getKeyByValue(field.getValues(), field.getFieldValue()));
            else {
                formData.put(field.getName(), field.getFieldValue());
            }
        }
        return new FormData(formData);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DynamicForm that = (DynamicForm) o;

        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        return fields != null ? fields.equals(that.fields) : that.fields == null;

    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (fields != null ? fields.hashCode() : 0);
        return result;
    }


}
