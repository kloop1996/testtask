package by.bsuir.kloop1996.testtask.viewModel;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.view.View;

import java.util.List;
import java.util.concurrent.TimeUnit;

import by.bsuir.kloop1996.testtask.Constants;
import by.bsuir.kloop1996.testtask.R;
import by.bsuir.kloop1996.testtask.TestTaskApplication;
import by.bsuir.kloop1996.testtask.data.DataManager;
import by.bsuir.kloop1996.testtask.data.TestTaskService;
import by.bsuir.kloop1996.testtask.model.DynamicForm;
import by.bsuir.kloop1996.testtask.model.Field;
import by.bsuir.kloop1996.testtask.model.FormData;
import by.bsuir.kloop1996.testtask.model.ResultOperation;
import by.bsuir.kloop1996.testtask.util.DialogFactory;
import by.bsuir.kloop1996.testtask.util.NetworkUtils;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;


/**
 * Created by kloop1996 on 15.10.2016.
 */

public class DynamicFormViewModel implements ViewModel {
    private Context context;
    private DynamicForm dynamicForm;
    private ObservableField<String> titleForm;
    private CompositeSubscription subscriptions;


    private ObservableBoolean isDataLoaded;
    private DataListener dataListener;
    private DataManager mDataManager;
    private ProgressDialog progressDialog;
    private Dialog errorDialog;

    private boolean isStateLoading = false;
    private boolean isStateSending = false;

    public DynamicFormViewModel(Context context, DataListener dataListener) {
        this.context = context;
        this.dataListener = dataListener;
        titleForm = new ObservableField<String>();
        subscriptions = new CompositeSubscription();
        isDataLoaded = new ObservableBoolean(false);

        mDataManager = TestTaskApplication.get(context).getComponent().dataManager();
    }

    public void onRefresh() {
        loadDynamicForm();
    }

    @Override
    public void destroy() {
        if (errorDialog != null && errorDialog.isShowing()) {
            errorDialog.cancel();
        }

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }

        context = null;

    }

    public DynamicForm getDynamicForm() {
        return dynamicForm;
    }

    public ObservableField<String> getTitleForm() {
        return titleForm;
    }

    public void setTitleForm(ObservableField<String> titleForm) {
        this.titleForm = titleForm;
    }

    public void loadDynamicForm() {

        showDialogLoad();
        isStateLoading = true;

        if (subscriptions != null && !subscriptions.isUnsubscribed()) {
            subscriptions.clear();
        }
        subscriptions.add(mDataManager.getTestTaskService().getDynamicForm()
                .delaySubscription(Constants.DELAY_LOAD_FORM_MILISECONDS, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(mDataManager.getSubscribeScheduler())
                .subscribe(new Subscriber<DynamicForm>() {
                    @Override
                    public void onCompleted() {
                        isStateLoading = false;
                        dataListener.onDynamicFormChanged(dynamicForm.getFields());
                        titleForm.set(dynamicForm.getTitle());
                        progressDialog.cancel();

                    }

                    @Override
                    public void onError(Throwable e) {
                        progressDialog.cancel();
                        isStateLoading = false;
                        errorDialog = DialogFactory.createSimpleOkErrorDialog(context, context.getString(R.string.error_getting_form));
                        errorDialog.show();
                    }

                    @Override
                    public void onNext(DynamicForm dynamicForm) {
                        isDataLoaded.set(true);
                        DynamicFormViewModel.this.dynamicForm = dynamicForm;
                    }
                }));

    }

    public void onClickFloatActionButton(View view) {
        if (NetworkUtils.isNetworkAvailable(context)) {
            getResultOperation();
        } else {
            errorDialog = DialogFactory.createSimpleOkErrorDialog(context, context.getResources().getString(R.string.error_connection));
            errorDialog.show();
        }
    }

    public void onClickRefresh(View view) {
        if (NetworkUtils.isNetworkAvailable(context)) {
            loadDynamicForm();
        } else {
            errorDialog = DialogFactory.createSimpleOkErrorDialog(context, context.getResources().getString(R.string.error_connection));
            errorDialog.show();
        }
    }

    private void getResultOperation() {
        if (dynamicForm != null) {
            FormData formData = dynamicForm.getFormData();

            isStateSending = true;
            showDialogSend();

            if (subscriptions != null && !subscriptions.isUnsubscribed()) {
                subscriptions.clear();
            }
            subscriptions.add((mDataManager.getTestTaskService().getResultOperation(formData)
                    .delaySubscription(Constants.DELAY_SEND_FORM_MILISECONDS, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(mDataManager.getSubscribeScheduler())
                    .subscribe(new Subscriber<ResultOperation>() {
                        @Override
                        public void onCompleted() {
                            progressDialog.cancel();
                            isStateSending = false;
                        }

                        @Override
                        public void onError(Throwable e) {
                            progressDialog.cancel();
                            isStateSending = false;
                            errorDialog = DialogFactory.createSimpleOkErrorDialog(context, context.getString(R.string.error_getting_form_result));
                            errorDialog.show();
                        }

                        @Override
                        public void onNext(ResultOperation resultOperation) {
                            DialogFactory.createSimpleOkMessageDialog(context, resultOperation.getResult()).show();
                        }
                    })));
        }
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
        mDataManager = TestTaskApplication.get(context).getComponent().dataManager();
    }

    public DataListener getDataListener() {
        return dataListener;
    }

    public void setDataListener(DataListener dataListener) {
        this.dataListener = dataListener;
    }

    public ObservableBoolean getIsDataLoaded() {
        return isDataLoaded;
    }

    public void setIsDataLoaded(ObservableBoolean isDataLoaded) {
        this.isDataLoaded = isDataLoaded;
    }

    public void invalidate() {
        if (dynamicForm != null && !dynamicForm.getFields().isEmpty())
            dataListener.onDynamicFormChanged(dynamicForm.getFields());

        if (isStateLoading) {
            showDialogLoad();
        }

        if (isStateSending) {
            showDialogSend();
        }
    }

    private void showDialogLoad() {
        progressDialog = DialogFactory.createProgressLoadDialog(context, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                subscriptions.clear();
                isStateLoading = false;
            }
        }, R.string.dialog_load_title);
        progressDialog.show();
    }

    private void showDialogSend() {
        progressDialog = DialogFactory.createProgressLoadDialog(context, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                subscriptions.clear();
                isStateSending = false;
            }
        }, R.string.dialog_send_title);
        progressDialog.show();
    }

    public interface DataListener {
        void onDynamicFormChanged(List<Field> fields);
    }
}
