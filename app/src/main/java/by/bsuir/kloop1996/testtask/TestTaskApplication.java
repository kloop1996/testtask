package by.bsuir.kloop1996.testtask;

import android.app.Application;
import android.content.Context;

import by.bsuir.kloop1996.testtask.data.TestTaskService;
import by.bsuir.kloop1996.testtask.injection.component.ApplicationComponent;
import by.bsuir.kloop1996.testtask.injection.component.DaggerApplicationComponent;
import by.bsuir.kloop1996.testtask.injection.module.ApplicationModule;
import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by kloop1996 on 14.10.2016.
 */

public class TestTaskApplication extends Application {

    ApplicationComponent mApplicationComponent;

    public static TestTaskApplication get(Context context) {
        return (TestTaskApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }
}
