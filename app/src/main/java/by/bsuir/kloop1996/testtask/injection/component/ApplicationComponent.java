package by.bsuir.kloop1996.testtask.injection.component;

import android.app.Application;

import javax.inject.Singleton;

import by.bsuir.kloop1996.testtask.data.DataManager;
import by.bsuir.kloop1996.testtask.injection.module.ApplicationModule;
import by.bsuir.kloop1996.testtask.injection.module.DataManagerModule;
import by.bsuir.kloop1996.testtask.injection.scope.PerDataManager;
import by.bsuir.kloop1996.testtask.ui.activity.DynamicFormActivity;
import dagger.Component;

/**
 * Created by kloop1996 on 17.10.2016.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(DynamicFormActivity dynamicFormActivity);

    Application application();

    DataManager dataManager();
}