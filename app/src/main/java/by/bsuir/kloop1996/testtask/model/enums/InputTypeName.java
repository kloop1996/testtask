package by.bsuir.kloop1996.testtask.model.enums;

/**
 * Created by kloop1996 on 16.10.2016.
 */

public enum InputTypeName {
    NUMERIC, TEXT, LIST;
}
