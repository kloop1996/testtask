package by.bsuir.kloop1996.testtask.ui.view;

/**
 * Created by kloop1996 on 17.10.2016.
 */

import android.content.Context;
import android.databinding.BindingMethod;
import android.databinding.BindingMethods;
import android.util.AttributeSet;

@BindingMethods(@BindingMethod(type = android.support.v4.widget.SwipeRefreshLayout.class, attribute = "app:onRefresh", method = "setOnRefreshListener"))
public class SwipeRefreshLayout extends android.support.v4.widget.SwipeRefreshLayout {

    public SwipeRefreshLayout(Context context) {
        super(context);
    }

    public SwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}