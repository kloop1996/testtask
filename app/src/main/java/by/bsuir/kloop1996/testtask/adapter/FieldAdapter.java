package by.bsuir.kloop1996.testtask.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

import by.bsuir.kloop1996.testtask.R;
import by.bsuir.kloop1996.testtask.databinding.ItemFormBinding;
import by.bsuir.kloop1996.testtask.model.Field;
import by.bsuir.kloop1996.testtask.model.enums.InputTypeName;
import by.bsuir.kloop1996.testtask.viewModel.ItemFormViewModel;

/**
 * Created by kloop1996 on 15.10.2016.
 */

public class FieldAdapter extends RecyclerView.Adapter<FieldAdapter.FieldViewHolder> {
    private List<Field> fields = Collections.emptyList();

    @Override
    public FieldViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemFormBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_form,
                parent,
                false);

        return new FieldViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(FieldViewHolder holder, int position) {
        holder.bindFormItem(fields.get(position));
    }

    @Override
    public int getItemCount() {
        return fields.size();
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    public static class FieldViewHolder extends RecyclerView.ViewHolder {
        final ItemFormBinding binding;

        public FieldViewHolder(ItemFormBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bindFormItem(Field field) {
            binding.setViewModel(new ItemFormViewModel(itemView.getContext(), field));
        }
    }
}
