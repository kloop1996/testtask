package by.bsuir.kloop1996.testtask;

/**
 * Created by kloop1996 on 16.10.2016.
 */

public class Constants {
    public final static int DELAY_LOAD_FORM_MILISECONDS = 5000;
    public final static int DELAY_SEND_FORM_MILISECONDS = 5000;
    public final static String DATA_FRAGMENT = "data_fragment";
}
