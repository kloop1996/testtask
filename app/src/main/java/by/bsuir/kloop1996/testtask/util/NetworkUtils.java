package by.bsuir.kloop1996.testtask.util;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by kloop1996 on 17.10.2016.
 */

public class NetworkUtils {
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null;
    }
}
