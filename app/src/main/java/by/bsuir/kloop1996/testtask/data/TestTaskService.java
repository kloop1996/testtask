package by.bsuir.kloop1996.testtask.data;

import by.bsuir.kloop1996.testtask.model.DynamicForm;
import by.bsuir.kloop1996.testtask.model.FormData;
import by.bsuir.kloop1996.testtask.model.ResultOperation;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by kloop1996 on 14.10.2016.
 */

public interface TestTaskService {

    String ENDPOINT = "http://test.clevertec.ru";

    @POST("/tt/meta")
    public rx.Observable<DynamicForm> getDynamicForm();

    @POST("/tt/data")
    public rx.Observable<ResultOperation> getResultOperation(@Body FormData formRequest);


    class Factory {
        public static TestTaskService create() {
            Retrofit retrofit;
            retrofit = new Retrofit.Builder()
                    .baseUrl(ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(TestTaskService.class);
        }
    }
}
