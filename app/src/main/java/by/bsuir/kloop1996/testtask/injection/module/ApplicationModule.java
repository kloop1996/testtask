package by.bsuir.kloop1996.testtask.injection.module;

import android.app.Application;

import javax.inject.Singleton;

import by.bsuir.kloop1996.testtask.data.DataManager;
import dagger.Module;
import dagger.Provides;
import rx.Scheduler;

/**
 * Created by kloop1996 on 17.10.2016.
 */
@Module
public class ApplicationModule {
    protected final Application mAppliction;

    public ApplicationModule(Application application) {
        mAppliction = application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return mAppliction;
    }

    @Provides
    @Singleton
    DataManager provideDataManager() {
        return new DataManager(mAppliction);
    }

}
