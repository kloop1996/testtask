package by.bsuir.kloop1996.testtask.data;

import android.content.Context;

import javax.inject.Inject;

import by.bsuir.kloop1996.testtask.TestTaskApplication;
import by.bsuir.kloop1996.testtask.injection.component.DaggerDataManagerComponent;
import by.bsuir.kloop1996.testtask.injection.module.DataManagerModule;
import rx.Scheduler;

/**
 * Created by kloop1996 on 17.10.2016.
 */

public class DataManager {
    @Inject
    protected TestTaskService mTestTaskService;
    @Inject
    protected Scheduler mSubscribeScheduler;

    public DataManager(Context context) {
        injectDependencies(context);
    }

    public Scheduler getSubscribeScheduler() {
        return mSubscribeScheduler;
    }

    public void setSubscribeScheduler(Scheduler mSubscribeScheduler) {
        this.mSubscribeScheduler = mSubscribeScheduler;
    }

    public TestTaskService getTestTaskService() {
        return mTestTaskService;
    }

    public void setTestTaskService(TestTaskService mTestTaskService) {
        this.mTestTaskService = mTestTaskService;
    }

    protected void injectDependencies(Context context) {

        DaggerDataManagerComponent.builder()
                .applicationComponent(TestTaskApplication.get(context).getComponent())
                .dataManagerModule(new DataManagerModule())
                .build()
                .inject(this);
    }
}
