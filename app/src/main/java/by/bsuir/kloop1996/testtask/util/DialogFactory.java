package by.bsuir.kloop1996.testtask.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

import by.bsuir.kloop1996.testtask.R;
import by.bsuir.kloop1996.testtask.viewModel.ViewModel;

/**
 * Created by kloop1996 on 16.10.2016.
 */

public class DialogFactory {
    public static Dialog createSimpleOkMessageDialog(Context context, String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context)
                .setMessage(message)
                .setNeutralButton(R.string.dialog_action_ok, null);
        return alertDialog.create();
    }

    public static Dialog createSimpleOkErrorDialog(Context context, String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.dialog_error_title))
                .setMessage(message)

                .setNeutralButton(R.string.dialog_action_ok, null);
        return alertDialog.create();
    }

    public static ProgressDialog createProgressLoadDialog(Context context, DialogInterface.OnClickListener delegateCancel, int messageResId) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(messageResId));
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getResources().getString(R.string.dialog_negative_button_text), delegateCancel);


        return progressDialog;
    }

}
