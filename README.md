# README #

### The App ###
The sample app displays a list of fields for a given JSON and sends the data to the server

### Libraries used ###

* AppCompat, CardView and RecyclerView
* Dagger 2
* Data Binding
* RxJava & RxAndroid
* Retrofit 2